/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./public/**/*.{html,js}"],
  theme: {
    extend: {
      screens: {
        xs: "320px",
        sm: "480px",
        md: "768px",
        lg: "991px",
        xl: "1200px",
        "2xl": "1350px",
        modal_sm: "500px",
        modal_md: "800px",
        modal_lg: "1200px",
      },
    },
  },
  plugins: [],
};
