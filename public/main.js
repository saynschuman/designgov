const body = document.getElementById("body");
const lowVisionBtn = document.getElementById("lowvision-btn");
const searchBlock = document.getElementById("search-block");
const openSearchBlock = document.getElementById("open-search-block");
const closeSearch = document.getElementById("close-search");
const showPortalSearch = document.getElementById("show-portal-search");
const showActsSearch = document.getElementById("show-acts-search");
const portalSearch = document.getElementById("portal-search");
const actsSearch = document.getElementById("acts-search");
const submenu = document.getElementById("submenu");
const openSubmenu = document.getElementById("open-submenu");
const hasSubmenuLinks = document.getElementsByClassName("has-submenu");
const closeModalBtn = document.getElementById("close-modal");
const mobileSearchOpenBtn = document.getElementById("mobile-search-open");
const burger = document.getElementById("burger");
const closeMobileMenu = document.getElementById("close-mobile-menu");
const siteMapArrow = document.getElementById("site-map-arrow");
const increment = document.getElementById("increment");
const decrement = document.getElementById("decrement");
const letters = document.getElementById("letters");
const modal = document.getElementById("modal");

document.querySelectorAll(".lowvision-btn").forEach((item) => {
  item.addEventListener("click", () => {
    if (body.classList.contains("lowvision")) {
      body?.classList.remove("lowvision");
      letters?.classList.add("hidden");
      letters?.classList.remove("flex");
    } else {
      body?.classList.add("lowvision");
      letters?.classList.remove("hidden");
      letters?.classList.add("flex");
    }
  });
});

openSearchBlock?.addEventListener("click", () => {
  if (searchBlock?.classList.contains("hidden")) {
    searchBlock?.classList.remove("hidden");
  } else {
    searchBlock?.classList.add("hidden");
  }
});

closeSearch?.addEventListener("click", () => {
  searchBlock?.classList.add("hidden");
});

showPortalSearch?.addEventListener("click", () => {
  actsSearch?.classList.add("hidden");
  portalSearch?.classList.remove("hidden");
  showPortalSearch.classList.add("active-tab");
  showActsSearch.classList.remove("active-tab");
});

showActsSearch?.addEventListener("click", () => {
  actsSearch?.classList.remove("hidden");
  portalSearch?.classList.add("hidden");
  showActsSearch.classList.add("active-tab");
  showPortalSearch.classList.remove("active-tab");
});

openSubmenu?.addEventListener("click", () => {
  if (submenu?.classList.contains("hidden")) {
    submenu?.classList.remove("hidden");
    submenu?.classList.add("flex");
    openSubmenu?.classList.add("rotate-180");
  } else {
    submenu?.classList.add("hidden");
    openSubmenu?.classList.remove("rotate-180");
  }
});

document.querySelectorAll(".has-submenu").forEach((item, _, self) => {
  item.addEventListener("click", (event) => {
    if (event.target.classList.contains("active-menu-link")) {
      self.forEach((element) => element.classList.remove("active-menu-link"));
      document
        .querySelectorAll(".fixed-submenu")
        .forEach((element) => element?.classList.add("hidden"));
      return;
    }

    self.forEach((element) => element.classList.remove("active-menu-link"));
    item.classList.add("active-menu-link");
    const dataId = event.target.getAttribute("data-menu-id");
    const submenuToShow = document.getElementById(dataId);
    document
      .querySelectorAll(".fixed-submenu")
      .forEach((element) => element?.classList.add("hidden"));
    submenuToShow?.classList.remove("hidden");
  });
});

const closeModal = () => {
  modal?.classList.add("hidden");
  modal?.classList.remove("block");
};

const openModal = () => {
  modal?.classList.remove("hidden");
  modal?.classList.add("block");
};

document.querySelectorAll(".open-modal-btn").forEach((item) => {
  item?.addEventListener("click", () => openModal());
});

closeModalBtn?.addEventListener("click", () => closeModal());

mobileSearchOpenBtn?.addEventListener("click", () => {
  searchBlock?.classList.remove("hidden");
});

burger?.addEventListener("click", () => {
  document.getElementById("mobile-menu")?.classList.remove("hidden");
});

closeMobileMenu?.addEventListener("click", () => {
  document.getElementById("mobile-menu")?.classList.add("hidden");
});

document.querySelectorAll(".has-mobile-submenu").forEach((item) => {
  item.addEventListener("click", (event) => {
    document
      .querySelectorAll(".mobile-sublement")
      .forEach((element) => element?.classList.add("hidden"));

    const dataId = event.target.getAttribute("data-menu-id");
    const submenuToShow = document.getElementById(dataId);
    submenuToShow?.classList.remove("hidden");
  });
});

document.querySelectorAll(".close-current").forEach((item) => {
  item.addEventListener("click", () => {
    document
      .querySelectorAll(".mobile-sublement")
      .forEach((element) => element?.classList.add("hidden"));
  });
});

siteMapArrow?.addEventListener("click", () => {
  const siteMap = document.getElementById("site-map");
  const siteMapIcon = document.getElementById("site-map-icon");
  if (siteMap.classList.contains("hidden")) {
    siteMap?.classList.remove("hidden");
    siteMapIcon.classList.add("rotate-180");
  } else {
    siteMap?.classList.add("hidden");
    siteMapIcon.classList.remove("rotate-180");
  }
});

var fontSize = 18;

increment?.addEventListener("click", () => {
  fontSize++;
  body.style.fontSize = `${fontSize + 1}px`;
});

decrement?.addEventListener("click", () => {
  fontSize--;
  body.style.fontSize = `${fontSize - 1}px`;
});

window.addEventListener("click", () => {
  document.querySelectorAll(".fixed-submenu").forEach((item) => {
    item.classList.add("hidden");
  });

  document.querySelectorAll(".has-submenu").forEach((item) => {
    item.classList.remove("active-menu-link");
  });

  searchBlock?.classList.add("hidden");
});

document.querySelectorAll(".fixed-submenu").forEach((item) => {
  item.addEventListener("click", (event) => {
    event.stopPropagation();
  });
});

document.querySelectorAll(".has-submenu").forEach((item) => {
  item.addEventListener("click", (event) => {
    event.stopPropagation();
  });
});

searchBlock.addEventListener("click", (event) => event.stopPropagation());
openSearchBlock.addEventListener("click", (event) => event.stopPropagation());

modal.addEventListener("click", () => closeModal());

document
  .getElementById("modal-inner")
  .addEventListener("click", (e) => e.stopPropagation());
